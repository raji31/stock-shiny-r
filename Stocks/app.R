library(dplyr)
library(RMySQL)
library(DT)
library(ggplot2)
library(shiny)
library(corrplot)
library(reshape2)

# Reduce warnings
options(warn=1)

# Change working directory
if (Sys.info()['sysname']=="Windows"){
    desktop<-gsub("\\\\","/", paste0(Sys.getenv("USERPROFILE"),"/desktop/"))
    setwd(paste0(desktop,"Stock-Shiny"))
} else if (Sys.info()['sysname']=="Linux"){
    setwd("/srv/shiny-server/stock-shiny-r")
}


trim <- function (x) gsub("^\\s+|\\s+$", "", x)
credentials<-read.table("db credentials.txt",stringsAsFactors = F)
credentials$V1<-trim(credentials$V1)

# Create connection to db
con<- dbConnect(MySQL(), host=credentials[1, ],
                user=credentials[2, ],
                password=credentials[3, ], 
                db="stock")

ui<-fluidPage(
    titlePanel("Card Stock Visualization"),
    selectInput("stock","Stock:" , choices = c("American Express"="AXP", "Visa" = "V", "JP Morgan & Chase"= "JPM", "Discover Financial Services" = "DFS", "MasterCard" = "MA",
                                               "Capital One" = "COF", "Citigroup"="C"), selected = "MA",
                multiple = T),
    dateRangeInput("dateRange", "Date Range:",
                   start = "2018-01-01",
                   end=Sys.Date()-1),
    DT::dataTableOutput("tbl"),
    downloadButton("downloader", "Download Data"),
    plotOutput("closePlot"), # Plot of the closing prices historicals for stocks selected
    plotOutput("correlationPlot") # Heat plot of the correclations between stocks

)


server<-function(input, output){
    # Grab teh data from stocks selected in SelectInput
    stk<-reactive({ con %>% 
            tbl("card_stock") %>% 
            filter(date>=local(input$dateRange[1])  & stock %in% local(input$stock)) %>%
            as.data.frame()
        })
    
    # Render the data in form of a data table
    output$tbl<-DT::renderDataTable(stk())
    
    # Render the closing prices of the stockes selected 
    output$closePlot <- renderPlot({
        
        data<-stk()
        data$date<-as.Date(data$date)
        ggplot(data=data, aes(x=date, y=close,color=stock))+
            geom_line()+
            geom_point()+
            ggtitle(paste0("Closing Prices of ", paste(input$stock,collapse = " ")),   )+
            scale_x_date(date_breaks = "6 month", date_labels = "%b")+
            scale_color_manual(values=c( "black", "red", "blue","purple","orange","green"))+
            theme(plot.title = element_text(hjust = 0.5, size=24, face="bold"))
    })
    
    # Add the download data feature
    output$downloader <- downloadHandler(
        filename = function(){
            paste("Stocks.csv")
        },
        content = function(file){
            output.data <-stk()
            write.csv(output.data,file)
        })
    
    # Heatmap of the different stock correlations selected based on closing price
    output$correlationPlot <- renderPlot({
            data<-stk()
            data<-dcast(data,date~stock, value.var="close",fun.aggregate = sum)
            data<-subset(data, select = -c(date))
            res<-melt(cor(data))
            ggplot(res, aes(x=Var1, y=Var2, fill=value))+geom_tile(color="white")+
                ggtitle("Correlation Plot")+
                xlab("Stock 1")+
                ylab("Stock 2")+
                theme_minimal()+
                theme(axis.text.x = element_text(angle = 45, vjust = 1, size = 12, hjust = 1),
                      plot.title = element_text(hjust = 0.5, size=24, face="bold"))+
                coord_fixed()
    })
}

# Close all db connects when app is exited
onStop(function(){
    lapply(dbListConnections( dbDriver( drv = "MySQL")), dbDisconnect)
})


# Run the application 
shinyApp(ui = ui, server = server)





