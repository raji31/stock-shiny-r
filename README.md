# stock-shiny-r

This project is a utlility designed in `R` using `RShiny` that fetches daily stock data for various card stocks such as Visa which is stored in a MySQL database and users can do the following

	1. Pick different credit card stocks and view their historical data such as closing price.
	2. Download the data extracts of the stocks they chosen in CSV format.
	3. View correlations between the stocks picked based on closing price. 
	
This project is hosted on a Linode server using `Shiny Server` as the backend server to handle R code execution and `nginx` to handle the incoming web requests.  

For more information on how to setup R shiny on a Linux based server you may refer to the sites below: 

	1. https://www.linode.com/docs/development/r/how-to-deploy-rshiny-server-on-ubuntu-and-debian/
	2. https://www.santoshsrinivas.com/shiny-server-on-ubuntu/
	3. https://support.rstudio.com/hc/en-us/articles/115003717168-Shiny-Server-Error-Logs
	4. https://www.digitalocean.com/community/tutorials/how-to-set-up-shiny-server-on-ubuntu-16-04
	
**Additional Comments:**

1. If your server is using a firewall such as **`ufw`** allow ports `3838` and `3838\tcp` to be opened as this is the ppoirt that R Shiny uses. 
2. Search your logs located in  `/var/log/shiny-server.log` and `/var/log/shiny-server/*.log` that should provide more information about any errors that is preventing R Shiny from operating. 
3. Run `sudo systemctl restart shiny-server` in case you make any changes to your R Shiny project and need to restart the Shiny server. 

	